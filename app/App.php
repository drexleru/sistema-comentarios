<?php
    class App{
        function __construct()
        {
            spl_autoload_register(function($className){
                include_once __DIR__ . "/../app/controllers/{$className}.php";
            });
        }

        public function run(){
            $request_uri = $_SERVER['REQUEST_URI'];
            $parts_uri = array_values(array_filter(explode('/', $request_uri)));
            $instance = null;
            $className = '';
            if(count($parts_uri) == 0){
                $c = new HomeController();
                $c->viewName = 'index';
                call_user_func([$c, 'index']);
                exit;
            }else if(count($parts_uri) == 1){
                $c = new HomeController();
                if(!method_exists($c, $parts_uri[0])) die("Pagina no existe");
                $c->viewName = $parts_uri[0];
                call_user_func([$c, $parts_uri[0]]);
                exit;
            }else if(count($parts_uri) == 2){
                $part_1 = ucfirst($parts_uri[0]);
                $part_2 = $parts_uri[1];
                $className = "{$part_1}Controller";
                $c = new $className;
                if(is_int($part_2)){
                    $c->viewName = 'index';
                    call_user_func([$c, 'index'], $part_2);
                }else{
                    if(!method_exists($c, $part_2)) die("Pagina no existe");
                    $c->viewName = $part_2;
                    call_user_func([$c, $part_2]);
                }
                exit;
            }else if(count($parts_uri) == 3){
                $part_1 = ucfirst($parts_uri[0]);
                $part_2 = $parts_uri[1];
                $part_3 = $parts_uri[2];
                $className = "{$part_1}Controller";
                $c = new $className;
                $c->viewName = $part_2;
                if(!method_exists($c, $part_2)) die("Pagina no existe");
                call_user_func([$c, $part_2], $part_3);
                exit;
            }

            die("Pagina no existe");

        }
    }
