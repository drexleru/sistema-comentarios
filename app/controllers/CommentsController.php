<?php

use App\Core\BaseController;
use App\Models\Comment;
use App\Core\Session;
use App\Core\ViewHelpers;

class CommentsController extends BaseController{
    public function add($post_id){
        $postData = $this->requestBody();
        $comment = new Comment;
        $result = $comment->insert(array_merge([
            'content' => $postData['comment'],
            'post_id' => $post_id
        ], Session::get('auth')));
        $gotComment = Comment::get($result['id']);
        $gotComment['date'] = _date($gotComment['created_at']);
        $gotComment['time'] = _time($gotComment['created_at']); 
        $template = ViewHelpers::_renderComment($gotComment, Session::get('auth')['commentator_email'], !is_null($gotComment['comment_parent_id']));
        echo json_encode(['result'=>$result, 'template'=>$template]);
    }

    public function answer($comment_id){
        $postData = $this->requestBody();
        $comment = new Comment;
        $result = $comment->insert(array_merge([
            'content' => $postData['comment'],
            'post_id' => $postData['post_id'],
            'comment_parent_id' => $comment_id
        ], Session::get('auth')));
        $gotComment = Comment::get($result['id']);
        $gotComment['date'] = _date($gotComment['created_at']);
        $gotComment['time'] = _time($gotComment['created_at']); 
        $template = ViewHelpers::_renderComment($gotComment, Session::get('auth')['commentator_email'], !is_null($gotComment['comment_parent_id']));
        echo json_encode(['result'=>$result, 'template'=>$template]);
    }


    public function like($id){
        $gotComment = Comment::get($id);
        $comment = new Comment;
        $result = $comment->updateVotes(['votes'=>intval($gotComment['votes'])+1, 'id'=>$id]);
        $votes = Comment::get($id)['votes'];
        echo json_encode(compact('result', 'votes'));
    }

    public function unlike($id){
        $gotComment = Comment::get($id);
        $comment = new Comment;
        $votes = intval($gotComment['votes'])-1;
        $votes = $votes <= 0?0:$votes;
        $result = $comment->updateVotes(['votes'=>$votes, 'id'=>$id]);
        $votes = Comment::get($id)['votes'];
        echo json_encode(compact('result', 'votes'));
    }
}