<?php

use App\Core\BaseController;
use App\Core\Session;
use App\Models\Post;
class HomeController extends BaseController{
    public function index($id=""){
        $this->titlePage = "Home";
        $this->render();
    }

    public function logout(){
        if($_SERVER['REQUEST_METHOD'] === 'POST'){
            Session::destroy();
            header('Location: /', false);
            die;
        }
        header('Location: '.$_SERVER["HTTP_REFERER"]);
        die;
 
    } 
}