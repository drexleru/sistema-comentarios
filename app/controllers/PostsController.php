<?php

use App\Core\BaseController;
use App\Models\Post;
use App\Models\Comment;

use App\Core\Session;
class PostsController extends BaseController{
    public function index(){
        $this->titlePage = "Posts";
        $posts = Post::all();
        $postData = $this->requestBody();
        if(!empty($postData))
            Session::set('auth', $postData);
        $this->render(array_merge(compact('posts')));
    }

    public function view($id=""){
        $post = Post::get($id);
        $comments = Comment::all($post['id']);
        $this->render(compact('post', 'comments'));
    }
}