<?php 
namespace App\Core;
Class BaseController{
    public $viewName;
    private $classFolder;
    private $conn;
    public $titlePlage;
    protected $auth;
    function __construct(){
        Session::start();
        $this->titleApp = "CarsCom";
        $this->titlePage = "Index";
        $this->classFolder = strtolower(str_replace('Controller', '', get_called_class()));
        if(!empty(Session::get('auth')) && '/posts/index' !== $_SERVER['REQUEST_URI'] && $_SERVER['REQUEST_URI'] == "/"){
            header('Location: '.'/posts/index', false);
            exit();
        }
        
    }
    protected function render($data=[]){
        $this->auth = Session::get('auth');
        if(!empty($data)) extract($data);

        $titleApp = $this->titleApp;
        $titlePage = $this->titlePage;
        $auth = $this->auth;

        if('/' !== $_SERVER['REQUEST_URI'] && empty(Session::get('auth'))){
            header('Location: /', false);
            exit();    
        }
        
        ob_start();
        include __DIR__ . "/../views/{$this->classFolder}/{$this->viewName}.php";
        $content = ob_get_clean();
        require_once __DIR__ . "/../views/layouts/main.php";
    }

    protected function db(){
        return $this->conn;
    }

    protected function requestBody(){
        return $_POST;
    }
}