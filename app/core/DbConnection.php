<?php

namespace App\Core;

class DbConnection
{
    private static $instance;
    private $dbh;
    public $executed;
    private function __construct()
    {
        try {
            $db = require_once(__DIR__ . '/../../config/db.php');
            extract($db);
            $this->dbh = new \PDO("mysql:host={$host};dbname={$dbName}", $user, $password);
            $this->dbh->exec("SET CHARACTER SET utf8");

        } catch (PDOException $e) {

            print "Error!: " . $e->getMessage();

            die();
        }
    }

    public function run($sql, $params=[])
    {
        $p = $this->dbh->prepare($sql);
        $p->setFetchMode(\PDO::FETCH_ASSOC);
        if(!empty($params))
            $this->executed = $p->execute($params);
        else
            $this->executed = $p->execute();
        return $p;

    }
 
    public static function conn()
    {

        if (!isset(self::$instance)) {
            $myClass = __CLASS__;
            self::$instance = new $myClass;
        }

        return self::$instance;
        
    }

    public function lastID(){
        return $this->dbh->lastInsertId();
    }


     // Prevent to clone object
    public function __clone()
    {
        trigger_error('Clone this  object not allow', E_USER_ERROR);
    }
}