<?php
namespace App\Core;
use App\Core\DbConnection;

class Model{
    protected static function conn(){
        return DbConnection::conn();
    }

    protected function stringColumns($data){
        return implode(', ', array_keys($data));
    }

    protected function stringBindColumns($data){
        return  ":" . implode(', :', array_keys($data));
    }
}