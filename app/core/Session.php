<?php 
namespace App\Core;

class Session{
    public static $sessionStarted;
    
    public static function start(){
        if(self::$sessionStarted == false){
            session_start();
            self::$sessionStarted = true;
        }
    }

    public static function set($key, $value){
        $_SESSION[$key] = $value;

    }

    public static function get($key){
        if(isset($_SESSION[$key])){
            return $_SESSION[$key];
        }

        return false;
    }

    public static function display(){
        _dump($_SESSION, false);
    }

    public static function destroy(){
        if(self::$sessionStarted){
            session_unset();
            session_destroy();
            self::$sessionStarted = false;
        }
    }
}