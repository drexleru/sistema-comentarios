<?php 

namespace App\Core;
use App\Models\Comment;
class ViewHelpers{
    private const ACTIONS_TEMPLATE = "
        <a href=\"/comments/like/{{id}}\"  data-toggle=\"tooltip\" data-placement=\"top\" title=\"Me gusta\" class=\"hand btn btn-success btn-sm round\"><i class=\"fa fa-thumbs-up \"></i></a>
        <a href=\"/comments/unlike/{{id}}\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Ya no me gusta\" class=\"hand btn btn-danger btn-sm round\"><i class=\"fa fa-thumbs-down \"></i></a>
    ";
    private const COMMENT_PARENT_TEMPLATE = "
        <div class=\"comment-item\">
            <div class=\"parent\" id=\"comment-item-{{id}}\">
                <div class=\"comment-header\">
                    <strong class=\"commentator-name\"><i class=\"fa fa-user-circle-o\"></i> {{commentator_name}}</strong>
                    <div class=\"actions\">
                        <a href=\"/comments/answer/{{id}}\"  data-toggle=\"tooltip\" data-placement=\"top\" title=\"Responder\" class=\"answer-comment btn btn-info btn-sm round\"><i class=\"fa fa-commenting\"></i></a>
                        {{actions}}
                    </div>
                </div>
                <p class=\"comment-content\">{{content}}</p>
                <span class=\"meta\">
                    <i class=\"fa fa-calendar\"></i> {{date}} - 
                    <i class=\"fa fa-clock-o\"></i> {{time}}
                </span>
                <span class=\"meta\"> <i class=\"fa fa-thumbs-up\"></i> <span class=\"votes\">{{votes}}</span></span> 
            </div>
            <div class=\"children\">
                {{child}}
            </div>
        </div>
    ";
    private const COMMENT_CHILDREN_TEMPLATE = "
        <div class=\"comment-item-child\">
            <div class=\"parent\" id=\"comment-item-{{id}}\">
                <div class=\"comment-header\">
                    <strong  class=\"commentator-name\"><i class=\"fa fa-user-circle-o\"></i> {{commentator_name}}</strong>
                    <div class=\"actions\">
                        <a href=\"/comments/answer/{{id}}\"  data-toggle=\"tooltip\" data-placement=\"top\" title=\"Responder\" class=\"answer-comment btn btn-info btn-sm round\"><i class=\"fa fa-commenting\"></i></a>
                        {{actions}}
                    </div>
                </div>
                <p class=\"comment-content\">{{content}}</p>
                <span class=\"meta\">
                    <i class=\"fa fa-calendar\"></i> {{date}} - 
                    <i class=\"fa fa-clock-o\"></i> {{time}}
                </span>
                <span class=\"meta\"> <i class=\"fa fa-thumbs-up\"></i> <span class=\"votes\">{{votes}}</span></span>                               
            </div>
            <div class=\"children\">
                {{child}}
            </div>
        </div>
    ";
    public static function _childComment($comment, $auth_email){
        $t_result = '';
        $comments = Comment::allChild($comment['post_id'], $comment['id']);
        
        foreach ($comments as $comment) {
            $result = preg_replace('/\{\{actions\}\}/', $comment['commentator_email']!=$auth_email?self::ACTIONS_TEMPLATE:'', self::COMMENT_CHILDREN_TEMPLATE);            
            $result = preg_replace('/\{\{commentator_name\}\}/', $comment['commentator_name'], $result);
            $result = preg_replace('/\{\{id\}\}/', $comment['id'], $result);      
            $result = preg_replace('/\{\{content\}\}/', $comment['content'], $result);
            $result = preg_replace('/\{\{date\}\}/', _date($comment['created_at']), $result);
            $result = preg_replace('/\{\{time\}\}/', _time($comment['created_at']), $result);
            $result = preg_replace('/\{\{votes\}\}/', $comment['votes'], $result);
            $result = preg_replace('/\{\{child\}\}/', self::_childComment($comment, $auth_email), $result);
            $t_result .= $result;
        }
        return $t_result;
    }

    public static function _renderComment($data, $auth_email, $children=false){
        $rComment = $children?self::COMMENT_CHILDREN_TEMPLATE:self::COMMENT_PARENT_TEMPLATE;
        $rComment = preg_replace('/\{\{actions\}\}/', $data['commentator_email']!=$auth_email?self::ACTIONS_TEMPLATE:'', $rComment);            

        foreach(array_keys($data) as $key){
            $rComment = preg_replace('/\{\{'.$key.'\}\}/', $data[$key], $rComment);
        }
        $rComment = preg_replace('/\{\{child\}\}/', '', $rComment);
        return $rComment;
    }
}

?>