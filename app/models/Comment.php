<?php
namespace App\Models;
use App\Core\Model;
class Comment extends Model{
    public function all($post_id){
        $q = self::conn()->run(
            'SELECT * FROM comments WHERE post_id = :post_id AND comment_parent_id IS NULL order by votes desc',
            [':post_id'=>$post_id]
        );
        $comments = $q->fetchAll();
        return $comments;
    }

    public function allChild($post_id, $comment_parent_id){
        $q = self::conn()->run(
            'SELECT * FROM comments WHERE post_id = :post_id AND comment_parent_id = :comment_parent_id order by votes desc',
            [':post_id'=>$post_id, ':comment_parent_id'=>$comment_parent_id]
        );
        $comments = $q->fetchAll();
        return $comments;
    }

    public function insert($data){
        $data['created_at'] = _now();
        $columns = $this->stringColumns($data);
        $bindColumns = $this->stringBindColumns($data);
        $q = self::conn()->run("INSERT INTO comments ({$columns}) VALUES({$bindColumns})", $data);
        return ['success'=> self::conn()->executed, 'errors'=>$q->errorInfo(), 'id'=>self::conn()->lastID()];
    }

    public static function get($id){
        $q = self::conn()->run('select * from comments where id = :id limit 1', [':id'=>$id]);
        $comment = $q->fetch();
        return $comment;
    }

    public function updateVotes($data){
        $data['updated_at'] = _now();
        $q = self::conn()->run("UPDATE comments SET updated_at=:updated_at, votes=:votes WHERE id=:id", $data);
        return ['success'=> self::conn()->executed, 'errors'=>$q->errorInfo()];
    }
}