<?php
namespace App\Models;
use App\Core\Model;
class Post extends Model{
    public static function all(){
        $q = self::conn()->run('select * from posts');
        $posts = $q->fetchAll();
        return $posts;
    }

    public static function get($id){
        $q = self::conn()->run('select * from posts where id = :id limit 1', [':id'=>$id]);
        $post = $q->fetch();
        return $post;
    }
}