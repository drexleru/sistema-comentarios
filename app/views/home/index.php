<section class="container">
    <div class="row">
        <div class="col main">
            <h1 class="w-100 badge badge-primary view-title">Sistema de comentarios</h1>
        </div>
    </div>
    <div class="row articles-list align-items-center justify-content-center">
        <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
            <div class="card mb-3">
                <div class="card-header text-white bg-info text-center"><h4>¿Dinos quien eres?</h4></div>
                <div class="card-body bg-default">
                    <form action="/posts/index" method="post">
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" class="form-control" id="name" name="commentator_name" required>
                        </div>
                        <div class="form-group">
                            <label for="email">E-Mail</label>
                            <input type="email" class="form-control" id="email" name="commentator_email" required>
                        </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                   </form>
                </div>
            </div>
        </div>
    </div>
</section>