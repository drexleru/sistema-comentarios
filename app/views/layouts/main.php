<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $titleApp ?> | <?= $titlePage ?></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/css/style.css"/>
</head>
<body>
    <?php if(!empty($this->auth)): ?>
    <div class="logout-button">
        <form action="/home/logout" method="post">
            <button type="submit" class="btn btn-info"><i class="fa fa-sign-out" aria-hidden="true"></i> Salir</button>
        </form>
    </div>
    <?php endif; ?>
    <?= $content ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script>

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

        $('.comment-parent-form form').on('submit', function(e){
            const $form = $(this);
            const url = $form.attr('action');
            const type = $form.attr('method');
            e.preventDefault();
            $.ajax({
                url,
                data: $form.serializeArray(),
                type,
                dataType: 'json',
                success: function(response){
                    const {result, template} = response;
                    if(result.success){
                        $form[0].reset();
                        $('.comment').prepend(template);
                        $('.comments-counter').text($('.comment-item').length);
                    }
                } 
            });
        });
        let $delegateElement = null;
        $(document).on('click', '.answer-comment', function(e){
            e.preventDefault();
            const comment = $(this).closest('.parent');
            const action = $(this).attr('href');
            $('#formAnswerComment').attr('action', action);
            $('#formAnswerComment').attr('data-el', `#${comment.attr('id')}`);
            $delegateElement = $(`#${comment.attr('id')}`);
            $('#answerModal').find('.modal-body').find('strong').html(comment.find('.commentator-name ').html());
            $('#answerModal').find('.modal-body').find('p').text(comment.find('.comment-content').text())

            $('#answerModal').modal('show');
        });

        $(document).on('submit', '#formAnswerComment', function(e){
            e.preventDefault();
            const $form = $(this);
            const url = $form.attr('action');
            const type = $form.attr('method');
            $.ajax({
                url,
                data: $form.serializeArray(),
                type,
                dataType: 'json',
                success: function(response){
                    const {result, template} = response;
                    if(result.success){
                        $form[0].reset();
                        $template = $(template);
                        $delegateElement.siblings('.children').prepend($template);
                        $('#answerModal').modal('hide');
                    }
                } 
            });
        });


        $(document).on('click', '.answer-comment', function(e){
            e.preventDefault();
            const comment = $(this).closest('.parent');
            const action = $(this).attr('href');
            $('#formAnswerComment').attr('action', action);
            $('#formAnswerComment').attr('data-el', `#${comment.attr('id')}`);
            $delegateElement = $(`#${comment.attr('id')}`);
            $('#answerModal').find('.modal-body').find('strong').html(comment.find('.commentator-name ').html());
            $('#answerModal').find('.modal-body').find('p').text(comment.find('.comment-content').text())

            $('#answerModal').modal('show');
        });

        $(document).on('click', '.hand', function(e){
            e.preventDefault();
            const $el = $(this);
            const url = $el.attr('href');
            const votes = parseInt($el.closest('.parent').find('.meta .votes').text());
            if(votes >= 0){
                $.ajax({
                    url,
                    type: 'post',
                    dataType: 'json',
                    success: function(response){
                        $el.closest('.parent').find('.meta .votes').text(response.votes);
                    } 
                });
            }

        });
  
    </script>

</body>
</html>