<section class="container">
    <div class="row">
        <div class="col main">
            <h1 class="w-100 badge badge-primary view-title">Lista de posts</h1>
        </div>
        <div class="col-md-12">
            <h5><i class="fa fa-user-circle"></i> <?= $auth['commentator_name'] ?></h5>
        </div>
        <div class="col-md-12">
            <h5><i class="fa fa-envelope"></i> <?= $auth['commentator_email']?></h5>
        </div>
    </div>
    <div class="row articles-list align-items-center justify-content-center">
        <?php foreach($posts as $post):?>
            <div class="post col-sm-12 col-md-12 col-lg-5 ">
                <div class="card">
                    <img class="card-img-top"  src="<?= $post['thumbnail'] ?>" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"><?= $post['title'] ?></h5>
                        <p class="card-text"><?= $post['sumary'] ?></p>
                        <a href="<?= "view/{$post['id']}" ?>" class="btn btn-primary">Leer más</a>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    </div>
</section>