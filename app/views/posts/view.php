<?php 
use App\Core\ViewHelpers;
?>

<div class="container">
    <section class="row justify-content-center">
       <article class="col-sm-12 col-lg-8">
            <h1><?= $post['title'] ?></h1>
            <span><?= $post['created_at'] ?></span>
            <figure>
                <img src="<?= $post['thumbnail'] ?>" alt="">
            </figure>
            <?= $post['content'] ?>
       </article>
    </section>
    <section class="row justify-content-center comments-container">
        <div class="col-sm-12 col-lg-8">
            <h2>Comentarios <span class="badge badge-primary comments-counter"><?= count($comments) ?></span></h2>
            <hr>
            <div class="comment-parent-form">
               <form action="<?= "/comments/add/{$post['id']}" ?>" method="post">
                    <textarea class="form-control" name="comment" placeholder="<?= "Hola {$auth['commentator_name']}, escribe un comentario" ?>" rows="3" required></textarea>
                    <button type="submit" class="btn btn-success">Enviar</button>
               </form>
            </div>
            <div class="comment">
                <?php foreach ($comments as $comment) : ?>
                    <div class="comment-item">
                        <div class="parent" id="comment-item-<?=$comment['id']?>">
                            <div class="comment-header">
                                <strong class="commentator-name"><i class="fa fa-user-circle-o"></i> <?= $comment['commentator_name'] ?></strong>
                                <div class="actions">
                                    <a href="<?= "/comments/answer/{$comment['id']}" ?>"  data-toggle="tooltip" data-placement="top" title="Responder" class="answer-comment btn btn-info btn-sm round"><i class="fa fa-commenting"></i></a>
                                    <?php if($comment['commentator_email'] != $auth['commentator_email']): ?>   
                                        <a href="<?= "/comments/like/{$comment['id']}" ?>" data-toggle="tooltip" data-placement="top" title="Me gusta" class="hand btn btn-success btn-sm round"><i class="fa fa-thumbs-up "></i></a>
                                        <a href="<?= "/comments/unlike/{$comment['id']}" ?>" data-toggle="tooltip" data-placement="top" title="Ya no me gusta" class="hand btn btn-danger btn-sm round"><i class="fa fa-thumbs-down "></i></a>
                                    <?php endif; ?>    
                                </div>
                            </div>
                            <p class="comment-content"><?= $comment['content'] ?></p>
                            <div class="comment-footer">
                                <div>
                                    <span class="meta">
                                        <i class="fa fa-calendar"></i> <?= _date($comment['created_at']) ?> - 
                                        <i class="fa fa-clock-o"></i> <?= _time($comment['created_at']) ?>
                                    </span>
                                    <span class="meta"> 
                                        <i class="fa fa-thumbs-up "></i> <span class="votes"> <?= $comment['votes'] ?></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="children">
                            <?= ViewHelpers::_childComment($comment, $auth['commentator_email']) ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="answerModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Responder al comentario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="comment-to-answer">
                    <strong></strong>
                    <p></p>
                </div>
                <form method="post" id="formAnswerComment">
                    <input type="hidden" value="<?= $post['id']?>" name="post_id">
                    <textarea class="form-control" name="comment" placeholder="<?= "Hola {$auth['commentator_name']}, escribe un comentario" ?>" rows="3" required></textarea>
                   
                </form>
            </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" form="formAnswerComment" class="btn btn-success">Responder</button>
            </div>
        </div>
    </div>
</div>

<style>
    .children{
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: flex-end;
    }
    .comment-footer{
        display: flex;
        justify-content: space-between;
        padding: 5px;
    }

    .comment-header{
        display: flex;
    }

    .comment-header .actions{
        margin-left: 5px;
    }

    .comment-to-answer{
        background-color: #eeeeee;
        padding: 10px 10px 1px 10px;
        margin-bottom: 10px;
    }

    .comment-to-answer p{
        margin-top: -5px;
        margin-left: 20px;
        margin-bottom: 5px;
    }

    .btn.btn-xs {
        padding  : .25rem .4rem;
        font-size  : .875rem;
        line-height  : .5;
        border-radius : .2rem;
    }

    .btn.round{
        border-radius: 100%;
    }
</style>