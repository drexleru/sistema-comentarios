-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 03-02-2019 a las 19:22:05
-- Versión del servidor: 10.1.34-MariaDB-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `blog`
--
CREATE DATABASE IF NOT EXISTS `blog` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `blog`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `commentator_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `commentator_email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `votes` int(11) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  `comment_parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `comments`
--

INSERT INTO `comments` (`id`, `content`, `commentator_name`, `commentator_email`, `votes`, `created_at`, `updated_at`, `post_id`, `comment_parent_id`) VALUES
(1, 'aaasas', 'Daniela Frade', 'danny@gmail.com', 0, '2019-02-03 09:02:59', NULL, 2, NULL),
(2, 'AAAA', 'Daniela Frade', 'danny@gmail.com', 0, '2019-02-03 09:03:04', NULL, 2, 1),
(3, 'SSSSS', 'Daniela Frade', 'danny@gmail.com', 0, '2019-02-03 09:03:08', NULL, 2, 2),
(4, 'weweweew', 'Carlos Porras', 'dreclerux@gmail.com', 0, '2019-02-03 09:31:46', NULL, 1, NULL),
(5, 'aaaa', 'Carlos Porras', 'dreclerux@gmail.com', 1, '2019-02-03 09:31:52', '2019-02-03 09:31:55', 1, 4),
(6, 'Super', 'Sofia Frade', 'sofi@gmail.com', 0, '2019-02-03 09:33:07', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sumary` text COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `posts`
--

INSERT INTO `posts` (`id`, `title`, `thumbnail`, `sumary`, `content`, `source`, `created_at`) VALUES
(1, 'La moto que no se cae, ¡ni chocando contra un camión!', 'https://cdn.autobild.es/sites/navi.axelspringer.es/public/styles/mosaic_default_v2/public/media/image/2017/12/lit-motors-c1-moto-electrica-que-no-cae-ni-chocando-camion.jpg?itok=Ua-y4LbM', 'La compañía Lit Motors ha inventado una moto eléctrica que nunca se cae. Sigue leyendo este artículo para comprobar que no nos lo estamos inventando.', '<p><span style=\"color: rgb(2, 2, 2); font-family: Lora; font-size: 16px; background-color: rgb(255, 255, 255);\"><strong>La compañía Lit Motors ha inventado una moto eléctrica que nunca se cae. Sigue leyendo este artículo para comprobar que no nos lo estamos inventando.</strong></span><br></p><span style=\"color: rgb(2, 2, 2); font-family: Lora; font-size: 16px; background-color: rgb(255, 255, 255);\"><strong><br></strong></span><p style=\"font-size: 16px; line-height: 27px; margin-top: 0px; margin-bottom: 25px; font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; color: rgb(2, 2, 2); background-color: rgb(255, 255, 255);\"><strong>La moto que no se cae, ¡ni chocando contra un camión!&nbsp;</strong>Este fabuloso invento de dos ruedas ha sido diseñado por la empresa estadounidense<strong>&nbsp;Lit Motors</strong>, que tiene su sede en San Francisco. Se trata de una moto eléctrica repleta de comodidades, aunque la principal sin duda es que nunca se cae.</p><p style=\"font-size: 16px; line-height: 27px; margin-top: 0px; margin-bottom: 25px; font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; color: rgb(2, 2, 2); background-color: rgb(255, 255, 255);\">¿Cómo es esto posible? Pues se debe a un sistema avanzando de giroscopios con el que es capaz de mantener el equilibrio de su estructura siempre y en todo momento, incluso cuando sufre algún accidente como por ejemplo un impacto lateral en un cruce. Ni por esas la&nbsp;<strong>C1</strong>, que es como se llama esta moto, termina en el suelo. ¿No te lo crees? Pues atento al siguiente vídeo:&nbsp;</p><p style=\"font-size: 16px; line-height: 27px; margin-top: 0px; margin-bottom: 25px; font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; color: rgb(2, 2, 2); background-color: rgb(255, 255, 255);\"><strong>La moto que no se cae, ¡ni chocando contra un camión!&nbsp;</strong>Este fabuloso invento de dos ruedas ha sido diseñado por la empresa estadounidense<strong>&nbsp;Lit Motors</strong>, que tiene su sede en San Francisco. Se trata de una moto eléctrica repleta de comodidades, aunque la principal sin duda es que nunca se cae.</p><p style=\"font-size: 16px; line-height: 27px; margin-top: 0px; margin-bottom: 25px; font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; color: rgb(2, 2, 2); background-color: rgb(255, 255, 255);\">¿Cómo es esto posible? Pues se debe a un sistema avanzando de giroscopios con el que es capaz de mantener el equilibrio de su estructura siempre y en todo momento, incluso cuando sufre algún accidente como por ejemplo un impacto lateral en un cruce. Ni por esas la&nbsp;<strong>C1</strong>, que es como se llama esta moto, termina en el suelo. ¿No te lo crees? Pues atento al siguiente vídeo:&nbsp;</p>', 'autobild.es,https://www.autobild.es/noticias/moto-que-no-cae-ni-chocando-camion-181028', '2019-02-02 11:17:36'),
(2, 'Vídeo: Tesla Model S vs Model 3 vs Model X vs Roadster', 'https://cdn.autobild.es/sites/navi.axelspringer.es/public/styles/mosaic_default_v2/public/media/image/2018/12/tesla-model-s.jpg?itok=42J76QkF', 'En esta carrera de aceleración se enfrenta el Tesla Model S contra el Tesla Model 3, el Tesla Model X y el antiguo Tesla Roadster. ¿Quién crees que ganará?', '<p><span style=\"color: rgb(2, 2, 2); font-family: Lora; font-size: 16px; background-color: rgb(255, 255, 255);\"><strong>En esta carrera de aceleración se enfrenta el Tesla Model S contra el Tesla Model 3, el Tesla Model X y el antiguo Tesla Roadster. ¿Quién crees que ganará?</strong></span><br></p><p><span style=\"color: rgb(2, 2, 2); font-family: Lora; font-size: 16px; background-color: rgb(255, 255, 255);\"><strong><br></strong></span><span style=\"color: rgb(2, 2, 2); font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px; background-color: rgb(255, 255, 255);\">A lo largo de los últimos años hemos visto como los productos de&nbsp;</span><a href=\"https://www.autobild.es/coches/tesla\" title=\"Tesla\" style=\"background-color: rgb(255, 255, 255); color: rgb(222, 2, 34); font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px;\">Tesla</a><span style=\"color: rgb(2, 2, 2); font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px; background-color: rgb(255, 255, 255);\">&nbsp;se han enfrentado a todo tipo de vehículos en carreras de aceleración, desde motocicletas a coches e incluso aviones de pasajeros. Sin embargo, hasta ahora no habíamos asistido a una carrera en al que los rivales fueran ellos mismos. En el siguiente vídeo podrás ver una&nbsp;</span><strong style=\"color: rgb(2, 2, 2); font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px; background-color: rgb(255, 255, 255);\">drag race que enfrenta al Tesla Model S contra el Tesla Model 3, el Tesla Model X y el antiguo Tesla Roadster</strong><span style=\"color: rgb(2, 2, 2); font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px; background-color: rgb(255, 255, 255);\">.</span><span style=\"color: rgb(2, 2, 2); font-family: Lora; font-size: 16px; background-color: rgb(255, 255, 255);\"><strong><br></strong></span></p><p style=\"font-size: 16px; line-height: 27px; margin-top: 0px; margin-bottom: 25px; font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; color: rgb(2, 2, 2); background-color: rgb(255, 255, 255);\">La gente detrás del canal de YouTube ‘<em>What’s Inside? Family</em>’, decidió alinear frente a una larga recta a los cuatro productos que ha lanzado la compañía de Elon Musk<strong>&nbsp;</strong>hasta ahora (a excepción del nuevo y poderoso Tesla Roadster que no llegará, presumiblemente, hasta el próximo año). Entonces, con un&nbsp;<strong>Tesla Model S P100D</strong>, un&nbsp;<strong>Tesla Model X P100D</strong>&nbsp;y un&nbsp;<strong>Tesla Model 3 Performance</strong>, la carrera ya parecía configurada.</p><h2 class=\"title\" style=\"margin-bottom: 25px; font-size: 20px; line-height: inherit; color: rgb(2, 2, 2); font-family: Lora; position: relative; z-index: 0; width: auto; background-color: rgb(255, 255, 255);\">Tesla Model S vs Model 3 vs Model X vs Roadster</h2><div class=\"video-container\"><iframe class=\"video\" width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/fqGxXfg3sZY\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen=\"\"></iframe></div><br><p><span style=\"color: rgb(2, 2, 2); font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px; background-color: rgb(255, 255, 255);\">Sin embargo, los organizadores decidieron añadir un cuarto participante, el Roadster original con el que la compañía se dio a conocer en el panorama de los coches. En el vídeo podrás ver enfrentarse al&nbsp;</span><a href=\"https://www.autobild.es/coches/tesla/model-s\" title=\"Tesla Model S\" style=\"background-color: rgb(255, 255, 255); color: rgb(222, 2, 34); font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px;\">Tesla Model S</a><span style=\"color: rgb(2, 2, 2); font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px; background-color: rgb(255, 255, 255);\">&nbsp;y al&nbsp;</span><a href=\"https://www.autobild.es/coches/tesla/model-x\" title=\"Tesla Model X\" style=\"background-color: rgb(255, 255, 255); color: rgb(222, 2, 34); font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px;\">Tesla Model X</a><span style=\"color: rgb(2, 2, 2); font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px; background-color: rgb(255, 255, 255);\">, ambos con más de 600 CV de potencia, al menos potente&nbsp;</span><a href=\"https://www.autobild.es/coches/tesla/model-3\" title=\"Tesla Model 3\" style=\"background-color: rgb(255, 255, 255); color: rgb(222, 2, 34); font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px;\">Tesla Model 3</a><span style=\"color: rgb(2, 2, 2); font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px; background-color: rgb(255, 255, 255);\">&nbsp;con 462 CV y al&nbsp;</span><a href=\"https://www.autobild.es/coches/tesla/roadster\" title=\"Tesla Roadster\" style=\"background-color: rgb(255, 255, 255); color: rgb(222, 2, 34); font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px;\">Tesla Roadster</a><span style=\"color: rgb(2, 2, 2); font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; font-size: 16px; background-color: rgb(255, 255, 255);\">&nbsp;con 292 CV.</span><br></p>', 'autobild.es,https://www.autobild.es/noticias/video-tesla-model-s-vs-model-3-vs-model-x-vs-roadster-356771', '2019-02-02 11:30:36');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_comment_2_post` (`post_id`),
  ADD KEY `comment_2_parent_comment` (`comment_parent_id`);

--
-- Indices de la tabla `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comment_2_parent_comment` FOREIGN KEY (`comment_parent_id`) REFERENCES `comments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_comment_2_post` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
