<?php
    include __DIR__ . '/../vendor/autoload.php';
    require_once __DIR__ . '/../app/App.php';
    require_once __DIR__ . '/../util/utils.php';
    $app = new App;
    $app->run();
?>