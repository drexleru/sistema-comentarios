<?php

function _dump($var, $die = true){
    echo "<pre>";
    print_r($var);
    echo "</pre>";
    if($die) die;
}

function _date($string){
    return date_format(date_create($string), 'Y-m-d');
}

function _time($string){
    return date_format(date_create($string), 'h:i a');
}

function _now(){
    return date("Y-m-d H:i:s");
}

